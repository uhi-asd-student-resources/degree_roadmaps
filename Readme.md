# Degree Roadmaps

This is a requested resource by students.  The degree is project-led which means that for 90% of the time your learning should be driven by the needs of your projects.  However, this may not be the only learning you wish to do.  It is also the case that some students like to have a guide as to the kind of things you might come across in the year.

Here is a description of what is in each directory:

* YearX - the roadmap for the year, this is not split by module as modules are only a bureaucratic requirement in this degree.
* suggested_solo_projects - projects that would bring on your own learning with a suggested difficulty

As always these are "live" documents that will change over time.
