# Technical Learning

## Technologies

Over the course of the first year you should have attempted the following technologies and got evidence of their usage either through project work, through online courses or programming challenges.

1. HTML
2. CSS
3. Javascript
4. Python
5. C++ or C
6. Java
7. SQL and Stored Procedures
8. Relational databases like DB2 or MySQL
9. NoSQL databases like Cloudant and MongoDB
10. Navigate and use IBM Cloud

## Algorithms & Data structures

Data structures you should be able to describe the pros and cons, including the Big-O notation, for:

1. Arrays
2. Hashmaps/dictionaries
3. Linked lists

Be able to tell the difference between these data structures in terms of performance in both time and memory, knowing the standard containers for these in the languages above.  You should also have an overview of trees and depth/breath first search algorithms.

## Tools

Be able to use and interpret the output of:
* Linters
* Formatting tools
* Compilers

## Testing

Be able to write small tests with a test runner in the chosen language.
Know what Test Driven Development (TDD) is.
Know what Behaviour Driven Development (BDD) is.

## Security

Be able to describe, explain and prevent.

* Cross-site scripting attacks
* Cross site request forgery attacks
* SQL Injection attacks



