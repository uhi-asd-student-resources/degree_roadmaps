# Year 1 - Weekly Questions / Prompts

These questions are not "homework" but prompts as to questions you might like to guide your learning over each week.  This is not the "only" thing you need to do or look at.  These are related to the learning outcomes only in that they may prompt you to write a reflection or do a recording on a topic. You don't have to look at these at all, or in the order they are given, they are just prompts to guide your development.

1. Why do we put such an emphasis on learning HTML, CSS and Javascript?  You might like to look at the number of job advers and salaries across technical areas.
2. How do HTML, CSS and Javascript work together to create a client side application?
3. Can you create a diagram with what happens from user click on a button in a browser for a message to go the server, run, a message to then get sent back and then a message to be given back to the user?  Go into as much depth as you can.
4. What is the difference between syntax and semantics in a programming language?  What is control flow? What are data structures? [LO1.1.1.5]
5. What other project management techniques are there, where are they still used? Why did agile development come about? [LO1.1.1.1]
6. Can you install and write some queries in SQL on a local MySQL server? [LO1.3.1.5]
7. What are the most common data structures and what are their properties? [LO1.1.2.5]
8. What does it mean to you to be a professional?  Can you think of behaviour that you would say is "unprofessional"?  Write about a time you were "unprofessional" and compare that with a time you were "professional"? [LO1.1.1.2]
9. Given we have compilers to check our code, why do we need to write tests? [LO1.1.3.5]
10. What is the software development cycle?  Give each part a score on where you think your understanding is at this point in time? [LO1.1.2.2]
11. What is a stored procedure?  Why do databases have them?  Do NoSQL databases have the capability of a stored procedure?  What does control flow look like in a stored procedure? [LO1.3.2.5]
12. What is entity integrity and referential integrity and in what sense are they important? Do you always need them? [LO1.3.1.5]
13. When you write comments, who do you write them for and what kind of things would be useful to know? [LO1.1.3.5]
14. Why might someone choose one web framework over another? What are some general criteria you could use to pick software for a project?  What might happen if you make the wrong choice?  Is it possible to make the right choice? [LO1.2.1.5]
15. Can you find blog posts talking about the makeup of a scrum team?  Compare this to the agile principles does it sound familiar or different? [LO1.3.1.1]
16. Why can you never have the best quality, cost and time altogether?  Can you name some products you own and highlight what tradeoffs they have made? [LO1.3.2.1]
17. Why is it important to get used to showing your unfinished work? [LO1.1.3.1]
18. Are software projects planned or do they evolve or both - discuss. [LO1.1.2.2]
19. Why is data described as the new oil or gold? [LO1.1.3.2]  
20. What capabilities are given in a database such as MySQL or Microsoft SQL to protect confidentiality, possession, integrity, authenticity and availability?  If not, why not?  What do you have to think about to capture these properties? [LO1.2.1.2]
21. Why is it not possible to create a 100% secure networked computer?  What threats does an air-gapped computer still have? [LO1.3.1.2]
22. Why is it important to think about security from the start of product development?  [LO1.1.2.2]
23. Draw out a diagram of what you want your career to look like?  How could you capture this in your PDP? Who are your idols and why? [LO1.2.1.3]
24. Can you think of anyone how has single handedly created a software product?  If you can, try and find out who he worked with?  Now re-evaluate whether he created the product by himself? [LO1.3.1.3]
25. Business's say they want good communicators.  What does this phrase mean to you?  Can you give 3 examples of when you communicated well and when you communicated poorly?  Compare and contrast each. [LO1.1.1.3]
26. What is quality assurance?  Why is it important?  What is the difference between total cost of ownership, lifecycle costing and price?  How does this affect quality assurance and also development tradeoffs? [LO1.2.1.4]