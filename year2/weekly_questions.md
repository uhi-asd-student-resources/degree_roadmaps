# Year 2 - Weekly Questions / Prompts

These questions are not "homework" but prompts as to questions you might like to guide your learning over each week.  This is not the "only" thing you need to do or look at.  These are related to the learning outcomes only in that they may prompt you to write a reflection or do a recording on a topic. You don't have to look at these at all, or in the order they are given, they are just prompts to guide your development.

1. Why do so many software projects fail or run over time and budget?  Can you identify 3 such public projects.  Are there lessons that you can learn from these with your smaller team projects? [LO2.4.1.1]
2. List all the processes, methods, techniques and tools you can find that you could use to manage a project. What are some of the common themes and what are some of the main differences in the approaches?  Why are there still products coming out to cater for this topic e.g. Monday.com? [LO2.6.1.1]
3. Setup a GitHub/Bitbucket/GitLabs repository with a simple hello world program.  Now attach as many services as you can find around quality assurance and show the "badges" on the Readme page.  [LO2.6.1.1]
4. How has your view of "professional" changed since starting in first year?  What has changed in your understanding of the knowledge a software developer requires to do their job? [LO2.4.1.2]
5. For your favourite product that you use on a daily basis, write out what the features are and what the associated benefits are?  Notice the different verbs you are using?  Are there an infinite number of benefits or are there a few that repeat over and over?  Could you come up with a marketing slogan for this product? [LO2.6.1.2]
6. Why do we have to worry about when and where data is located and what happens when its transferred from Europe to the US for instance? [LO2.6.2.2]
7. When signing up for a new service do you think about where the data you will be creating will be stored? Discuss. [LO2.6.2.2]
8. What makes a good presentation?  You have probably looked at many Youtube videos and ted talks.  What makes a good one?  Are there things you can learn and add to your own presentations? [LO2.4.1.3]
9. Why do business's to performance evaluation? Explore your own thoughts on whether these are good or bad for both employee and employer? Can you find internet resources that both agree with you but also some that disagree with you?  What are the tradeoffs that are made? [LO2.5.2.3]
10. Find a technical news story that interests you (theregister.co.uk is a good site to use).  Who are the big players in this part of the industry?Identify some industry literature on the direction of the industry?  What terms could you use then to relate it to academic literature? [LO2.6.2.3]
11. What is the difference between a thread and a process?  Why use one over another when you want to do tasks in parallel?  What is the Dining Philosopher's problem and how does it relate to parallel computation? [LO2.4.1.4]
12. How can data be anonymised but still processed in a way that is useful?  [LO2.6.1.4]
13. Put yourself in the place of a student with a disability.  Think of your favourite piece of software.  How does the software make itself available to that student?  In what ways does it fail to be accessible?  [LO2.4.1.5]
14. What are some real world problems that data mining and other machine learning techniques have been used for?  What are the common features?  For what problems do these techniques work well and which ones does it fail? [LO2.6.1.5]
15. What is the relationship between a standard library and a programming language? 
16. What are the different ways an operating system allows multiple programs to run at the same time? [LO2.6.2.3]


